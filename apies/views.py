from django.shortcuts import render
from .tools import get_sex, get_string_name
from .plagins import google_plus_search, vk_search


def index(request):
    if request.method == 'POST':
        post = request.POST.copy()
        string_name = post['q']
        google, vk, twitter, linked, fb = [], [], [], [], []
        if 'google' in post:
            google = google_plus_search(string_name)
        if 'vk' in post:
            vk = vk_search(string_name, post['age'], post['age'], post['sex'])
        return render(request, 'index.html', {'google': google, 'vk': vk})
    return render(request, 'index.html')
