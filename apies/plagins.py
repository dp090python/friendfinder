import vk
from googleapiclient.discovery import build


def google_plus_search(string_name):
    if string_name:
        service = build('plus', 'v1', developerKey='AIzaSyC3P4APPgx2Qew-tM6f98yXyDuJAMS0ROQ')
        people_resource = service.people()
        people_document = people_resource.search(
            maxResults=10, query=string_name).execute()
        users_list = []
        if 'items' in people_document:
            for user in people_document['items']:
                users = {}
                users['name'] = user['displayName']
                users['page'] = user['url']
                users['avatar'] = user['image']['url']
                users_list.append(users)
        if users_list:
            return users_list
        else:
            return None
    else:
        return None


def vk_search(string_name, age_from, age_to, sex=0):
    if age_from == '':
        age_from = 0
    else:
        age_from = int(age_from)
    if age_to == '':
        age_to = 100
    else:
        age_to = int(age_to)
    sex = int(sex)

    session = vk.Session()
    vkapi = vk.API(session,
                   access_token='b5ab20cde56735b15949c2cf66fa9b4465e19f793f4791d5e2b76fa3f6c0cbfc2c79dd6e84b7f6c48eaa7')
    if string_name:
        users = vkapi.users.search(q=string_name,
                                   age_from=age_from,
                                   age_to=age_to,
                                   sex=sex,
                                   count=10,
                                   fields=['photo_50', 'sex', 'bdate', 'domain'])
    else:
        users = vkapi.users.search(age_from=age_from,
                                   age_to=age_to,
                                   sex=sex,
                                   count=10,
                                   fields=['photo_50', 'sex', 'bdate', 'domain'])
    users_list = []
    users = users[1:]
    for user in users:
        vk_users = {}
        vk_users['first_name'] = user['first_name']
        vk_users['last_name'] = user['last_name']
        if 'bdate' in user:
            vk_users['bdate'] = user['bdate']
        else:
            vk_users['bdate'] = 'Birthday not set'
        vk_users['sex'] = user['sex']
        vk_users['photo'] = user['photo_50']
        vk_users['domain'] = user['domain']
        users_list.append(vk_users)

    if users_list:
        return users_list
    else:
        return None
