# Get up Application #

```

#!bash
* git clone -b develop "your link from the clone"
* cd friendfinder
* mkvirtualenv --no-site-packages -p python3 friendfinder
* pip install -r req.txt
* python manage.py migrate
* python manage.py runserver 
* link - http://localhost:8000/ 
* Be happy!

```